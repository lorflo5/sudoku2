		package sudoku.dialog;
		
		import java.awt.Color;
		import java.awt.Dimension;
		import java.awt.Graphics;
		import java.awt.event.MouseAdapter;
		import java.awt.event.MouseEvent;
		
		import javax.swing.JPanel;
		
		import sudoku.model.Board;
		
		/**
		 * A special panel class to display a Sudoku board modeled by the
		 * {@link sudoku.model.Board} class. You need to write code for
		 * the paint() method.
		 *
		 * @see sudoku.model.Board
		 * @author Yoonsik Cheon
		 */
		@SuppressWarnings("serial")
		public class BoardPanel extends JPanel {
			
			 public static Integer clickx;
			 public static int clicky;
			 
			private String aValues;
		    
			public interface ClickListener {
				
				/** Callback to notify clicking of a square. 
				 * 
				 * @param x 0-based column index of the clicked square
				 * @param y 0-based row index of the clicked square
				 */
				void clicked(int x, int y);
			}
			
		    /** Background color of the board. */
			private static final Color boardColor = new Color(247, 223, 150);
			
			private static final Color black = new Color(0,0,0);
		
		    /** Board to be displayed. */
		    private Board board;
		
		    /** Width and height of a square in pixels. */
		    private int squareSize;
		
		    /** Create a new board panel to display the given board. */
		    public BoardPanel(Board board, ClickListener listener) {
		        this.board = board;
		        addMouseListener(new MouseAdapter() {
		            public void mouseClicked(MouseEvent e) {
		            	int xy = locateSquaree(e.getX(), e.getY());
		            	if (xy >= 0) {
		            		listener.clicked(xy / 100, xy % 100);
		            		clickx = xy/100;
		            		clicky = xy % 100;
		            	}
		            }
		        });
		    }
		
		    /** Set the board to be displayed. */
		    public void setBoard(Board board) {
		    	this.board = board;
		    }
		    
		    /**
		     * Given a screen coordinate, return the indexes of the corresponding square
		     * or -1 if there is no square.
		     * The indexes are encoded and returned as x*100 + y, 
		     * where x and y are 0-based column/row indexes.
		     */
		    public int locateSquaree(int x, int y) 
		    {
		    	if (x < 0 || x > board.size * squareSize
		    			|| y < 0 || y > board.size * squareSize)
		    	{
		    		return -1;
		    	}
		    	int xx = x / squareSize;
		    	int yy = y / squareSize;
		    	
		    	
		    	board.getCoordinates(xx,yy);
		    	
		    	
		    	return xx * 100 + yy;
		    }
		
		    /** Draw the associated board. */
		    @Override
		    public void paint(Graphics g) 
		    {
		        super.paint(g); 
		
		        // determine the square size
		        Dimension dim = getSize();
		        squareSize = Math.min(dim.width, dim.height) / board.size;
		
		        // draw background
		        final Color oldColor = g.getColor();
		
		        
		        g.setColor(boardColor);
		        g.fillRect(0, 0, squareSize * board.size, squareSize * board.size);
		       
		        // WRITE YOUR CODE HERE ...
		        
		        // drawing grid ////////////////
		        g.setColor(black);
		     
		        int y = 0;
		        int x = 0;
		        int border = 0;
		        for (int i = 0; i<=board.size; i++)
		        {
		        	
		        	if(border==0) 
		        	{
		        	g.drawLine(0, x+1, squareSize * board.size,x+1);
		        	}
		        	
		        g.drawLine(0, x, squareSize * board.size,x);
		        border ++;
		        
		        while(border == Math.sqrt(board.size)+1)
		        {
		          g.drawLine(0, x+1, (board.size*squareSize), x+1);
		          border = 1;
		        }
		        
		        x += squareSize;
		        }
		        
		        border = 0;
		        
		        for (int i = 0; i<=board.size; i++) 
		        {
		        	
		        	if(border==0) 
		        	{
		        	g.drawLine(y+1, 0, y+1,squareSize * board.size);
		        	}
		        	
		            g.drawLine(y, 0, y,squareSize * board.size);
		            border ++;
		            
		           while(border == Math.sqrt(board.size)+1)
		            {
		            	g.drawLine(y+1, 0, y+1,squareSize * board.size);
		                border = 1;
		            }
		            
		            y += squareSize;
		          }
		        
		       //////////////////////////////////////////////////////////////////////////////
		        
		        //drawing values into board
		        
		        g.setColor(Color.BLACK);
		        
		        for(int i=0;i<board.size;i++)
		        {
		          for(int j=0;j<board.size;j++)
		          {
		        	  int value = board.getValue(j,i);
		        	  aValues = Integer.toString(value); 
		        	 
		        	  if(value != 0 )
		        	  {	  
		        		  if (aValues != null) 
		        		  {
		        		  if(board.checkRow(i,j,value) || board.checkCol(i, j, value)) //|| board.checkBox(i, j, value))
		        		  {
		        			  g.setColor(Color.RED);
		        			  g.drawString(aValues, j*squareSize+10, i*squareSize+20);
		        		  }
		        		  
		        		  else 
		        		  {
		        		  g.setColor(oldColor);
		                  g.drawString(aValues, j*squareSize+10, i*squareSize+20);
		        		  }
		        		  }
		        	  }
		          }
		        }
		        
		        //////////////////////////////////////////////////////////////////
		        
		        // square selected
		        
		        g.setColor(Color.CYAN); 
		        
		        if(clickx!=null) 
		        {
		        g.fillRect(clickx*(squareSize), clicky*(squareSize), squareSize, squareSize);
		        }
		        
		       ///////////////////////////////////////////////////////
		        // printing value
		        
		        g.setColor(Color.BLACK);
		        
		        
		        if (SudokuDialog.temp != null) 
		        {
		        	
		         g.drawString(SudokuDialog.temp, clickx*squareSize+10, (clicky*squareSize)+20);  
		       }
		        
		        
		    }
		
		}
