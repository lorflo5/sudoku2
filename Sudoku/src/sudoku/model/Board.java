package sudoku.model;
	
	/** An abstraction of Sudoku puzzle. 
	 * @author MannyBoy Flores
	 * @author Mark Nunez
	 * @version 3/4/2018
	 **/
	public class Board {
	
		public static int[][] array;
		private int[][] temp;
	
	    /** Size of this board (number of columns/rows). */
	    public int size;
	    
	    public int value;
	    
	    private int oldX;
	    private int oldY;
	    private int oldValue;
	
	    
	
	    /** Create a new board of the given size. */
	    public Board(int size) {
	        this.size = size;
	
	        // WRITE YOUR CODE HERE ...
	        
	        array = new int[size][size];
	        temp = new int[size][size];
	        
	    }
	
	    /** Return the size of this board. */
	    public int size() {
	    	return size;
	    }
	
	    // WRITE YOUR CODE HERE ..
	    /** Gets the value at this index.
	     * @param x
	     * @param y
	     * @return this value
	     */
	    public static int getValue(int x, int y) {
	    	
	    	return array[x][y];
	    }
	    /**
	     * Assigns this value to a particular index.
	     * @param x
	     */
	    public void setValue(int x)
	    {
	    	
	    	 for(int i=0;i<size;i++)
	         {
	           for(int j=0;j<size;j++)
	           {
	        	   if (temp[i][j] == -1) {
	        		   array[i][j] = x;
	        		   
	        	   }	        	   
	           }
	         }     
	    }
	    /**
	     * Gets the index, puts a temporary value in
	     * a temporary index position.
	     * @param x
	     * @param y
	     */
	    public void getCoordinates(int x, int y)
	    { 
	    	
	    	if(oldX >=0) {
	    	temp[oldX][oldY] = oldValue;
	    	}
	    	
	    	temp[x][y] = oldValue;
	      
	    	oldX = x;
	    	oldY = y;
	    	temp[x][y] = -1;
	    	
	      for(int i=0;i<size;i++)
	      {
	        for(int j=0;j<size;j++)
	        {
	          System.out.print(array[j][i]);
	        }
	        System.out.println();
	      }
	      System.out.println();
	
	    }
	    /**
	     * Sets all values in array to zero.
	     */
	    public void clear()
	    {
	      for(int i=0;i<size;i++)
	      {
	        for(int j=0;j<size;j++)
	        {
	          array[j][i] = 0;
	        }
	      }
	    }
	    
	 
	    
	  //--------------- CHECKS ROWS -------------------------------------
	    /**
	     * Checks the rows on board for duplicate values
	     * @param row
	     * @param col
	     * @param val
	     * @return true/false
	     */
	    public boolean checkRow(int row,int col, int val) 
	    {
	    	int tempV = array[col][row];
	    	array[col][row] = 0; 
	    	for (int i = 0; i < size; i++)
	        {
	            if (val == array[i][row])
	            {
	            	return true;   
	            }
	        }
	        array[col][row] = tempV; 
	        return false;
	    }
	//---------------------- CHECKS COLUMNS ---------------------------------------------- 
	    /**
	     * Checks the columns on the board for duplicate values
	     * @param row
	     * @param col
	     * @param val
	     * @return true/false
	     */
	    public boolean checkCol(int row, int col, int val) 
	    {
	    	int tempV = array[col][row];
	    	array[col][row] = 0;
	        for (int i = 0; i < size; i++)  // column
	            if (val == array[col][i])
	            { 
	            	return true;
	            }
	        array[col][row] = tempV; 
	        return false; }
	//------------------------- CHECKS BOX -------------------------------------------------- 
	    /**
	     * Checks the subgrids on the board for duplicate values
	     * @param row
	     * @param col
	     * @param val
	     * @return true/false
	     */
	    public boolean checkBox(int row, int col, int val)
	    {
	        int tempV = array[col][row];
	    	array[col][row] = 0;
	       for (int i = 0; i < Math.sqrt(size); i++) // box
	            for (int j = 0; j < Math.sqrt(size); j++)
	            {
	            	if(col+i <= col+1 && row+j <= row+1)
	                if(val == array[col+i][row+j])
	                {
	                    System.out.println(val + " is already in that box " );
	                     return true;
	                }
	            	if(col+i <= col +1 && row-j > row-1)
		                if(val == array[col+i][row-j])
		                {
		                    System.out.println(val + " is already in that box " );
		                     return true;
		                }
	            }
	        array[col][row] = tempV;
	        return false;

	    }
	    /**
	     * Checks to see if the board is solved
	     * @return true/false
	     */
	    public boolean complete()
	    {
	    	int count = 0;
	    	for (int i = 0; i < size; i++)
	    	{
	            for (int j = 0; j < size; j++)
	            {
	    	if(array[i][j] != 0)
	    		count++;
	            }
	         }
	    	if(count == Math.pow(size,2))
	    		return true;
	    	
	    	return false;
	    }
	}
		